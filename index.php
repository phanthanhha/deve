<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>AngularJS Insert Update Delete Using PHP MySQL</title>
        <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,300,200' rel='stylesheet' type='text/css'>	
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <style>
            .pointer span{
                cursor: pointer;
            }
        </style>
    </head>
    <body data-ng-app="postModule" data-ng-controller="PostController" data-ng-init="init()" class="ng-scope" cz-shortcut-listen="true">
        <input type="hidden" id="base_path" value="http://localhost/angular-php/">
        <div class="container">
            <div class="clearfix"></div>
            <h2 class="title text-center"> AngularJS Insert Update Delete Using PHP MySQL</h2>
            <div class="row mt80">
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 animated fadeInDown">
                    <div class="alert alert-danger text-center alert-failure-div" role="alert" style="display: none">
                        <p></p>
                    </div>
                    <div class="alert alert-success text-center alert-success-div" role="alert" style="display: none">
                        <p></p>
                    </div>
                    <form novalidate="" name="userForm" class="ng-pristine ng-invalid ng-invalid-required ng-valid-minlength ng-valid-email">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Name</label> 
                            <input data-ng-minlength="3" required="" type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-minlength ng-touched" id="name" name="name" placeholder="Name" data-ng-model="tempUser.name">
                            <span class="help-block error ng-binding ng-hide" data-ng-show="userForm.name.$invalid & amp; & amp; userForm.name.$dirty">
                                Please enter name
                            </span>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Email</label> 
                            <input data-ng-minlength="3" required="" type="email" class="form-control ng-pristine ng-untouched ng-valid-email ng-invalid ng-invalid-required ng-valid-minlength" id="email" name="email" placeholder="Email" data-ng-model="tempUser.email">
                            <span class="help-block error ng-binding ng-hide" data-ng-show="userForm.email.$invalid & amp; & amp; userForm.email.$dirty">
                                Please enter email
                            </span>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Company Name</label>  
                            <input data-ng-minlength="3" required="" type="text" class="form-control ng-pristine ng-untouched ng-invalid ng-invalid-required ng-valid-minlength" id="company_name" name="company_name" placeholder="Company Name" data-ng-model="tempUser.companyName">
                            <span class="help-block error ng-binding ng-hide" data-ng-show="userForm.company_name.$invalid & amp; & amp; userForm.company_name.$dirty">
                                Please enter company name
                            </span>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputFile">Designation</label> 
                            <input data-ng-minlength="3" required="" type="text" class="form-control ng-pristine ng-untouched ng-invalid ng-invalid-required ng-valid-minlength" id="designation" name="designation" placeholder="Designation" data-ng-model="tempUser.designation">
                            <span class="help-block error ng-binding ng-hide" data-ng-show="userForm.designation.$invalid & amp; & amp; userForm.designation.$dirty">
                                Please enter designation
                            </span>
                        </div>
                        <!-- <input type="hidden" data-ng-model='tempUser.id'>  -->
                        <div class="text-center">
                            <button ng-disabled="userForm.$invalid" data-loading-text="Saving User..." ng-hide="tempUser.id" type="submit" class="btn btn-save" data-ng-click="addUser()" disabled="disabled">Save User</button>
                            <button ng-disabled="userForm.$invalid" data-loading-text="Updating User..." ng-hide="!tempUser.id" type="submit" class="btn btn-save ng-hide" data-ng-click="updateUser()" disabled="disabled">Update User</button>
                        </div>
                    </form>
                    <div class="clearfix"></div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8 animated fadeInUp">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover table-striped">
                            <thead>
                                <tr>
                                    <th width="5%">#</th>
                                    <th width="20%">Name</th>
                                    <th width="20%">Email</th>
                                    <th width="20%">Company Name</th>
                                    <th width="15%">Designation</th>
                                    <th width="20%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <!-- ngRepeat: user in post.users | orderBy : '-id' -->
                                <tr data-ng-repeat="user in post.users| orderBy : 'id'" class="ng-scope">
                                    <th scope="row" class="ng-binding">{{ user.id}}</th>
                                    <td class="ng-binding"> {{ user.name}}</td>
                                    <td class="ng-binding"> {{ user.email}} </td>
                                    <td class="ng-binding">  {{ user.companyName}}</td>
                                    <td class="ng-binding">  {{ user.designation}}</td>
                                    <td class="pointer"> 
                                        <span data-ng-click="editUser(user)"> Edit</span> | <span data-ng-click="deleteUser(user)">Delete</span> 
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/angular.min.js"></script>
        <script type="text/javascript" src="js/angular-route.min.js"></script>
        <script type="text/javascript" src="js/script.js"></script>
    </body>
</html>
